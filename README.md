# workday-student-userstyles

This repository contains userstyles (and possibly userscripts) to fix various aspects of Workday Student, at least as implemented in UBC.

## Currently available
- Profile picture replacement.
- Hiding cumulative average.

## How to use
1. Install the [Stylus](https://github.com/openstyles/stylus) extension ([Firefox](https://addons.mozilla.org/en-US/firefox/addon/styl-us/), [Chrome](https://chromewebstore.google.com/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne)). It's probably also available for other browsers.
2. Click the extension icon, "Manage", "Write new style", then "Import".
3. Copy and paste the contents of the .css files into the textbox, then click "Overwrite style".


## No idea how to implement
- Animations. I think figuring out how to remove them is way beyond my ability right now. Help would be appreciated.
- Course scheduling. It would probably require an external scraper of some sort.

For any inquiries, yell at me at <https://reddit.com/u/lessquestionablename>.